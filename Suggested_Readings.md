Arithmetic of Listening: Tuning Theory and History for the Impractical Musician 
    by Kyle Gann

Harmonic Experience: Tonal Harmony from Its Natural Origins to Its Modern Expression
    by W. A. Mathieu

The Sounds of Music: Perception and Notation 
    by Gerald Eskalin

Tuning Timbre Spectrum Scale 
    by William Sethares
    Brief overview/1st chapter at: https://sethares.engr.wisc.edu/ttss.html

Just Intonation Primer by David Doty
    http://www.dbdoty.com/Words/Primer1.html
    
Music and Memory by Bob Snyder

Sweet Anticipation: Music and the Psychology of Expectation 
    By David Huron
