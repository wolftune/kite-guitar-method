#Kite-Tuning tools and how to use them:

##http://www.tallkite.com/alt-tuner.html: 
Alt-tuner is a DAW plug-in that retunes almost every midi keyboad or softsynth (developed by Kite Giedraitis) - can be used for the 41-EDO Kite Tuning as well as others.

##41EDO Ear Trainer (by Kite Giedraitis)
- a Reaper plugin with fantastic features

#41-edo.scl is a scala file in this repo for use with any scala compatible program
- Many of them are listed at https://en.xen.wiki/w/List_of_Microtonal_Software_Plugins
but only the ones that say "scl file import" under "tuning method" apply



Tuners:

EDOTUNER (folder in this repository) is a custom strobe tuner (made by Kite) for Reaper - 
download and follow the instructions at the top of EDOTUNER.txt in order to load and use

Lingot is a tuning software with a branch made by ibancg which imports the labels properly from the 41-edo.scl file,
you can find it here:
https://github.com/ibancg/lingot  (note: you will have to change the deviation of cents)

'





Please also refer to other primary resources at:

www.tallkite.com

https://en.xen.wiki/w/The_Kite_Guitar

